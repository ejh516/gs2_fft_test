module communications
!==============================================================================#
! COMMUNICATIONS
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-28
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use constants
  use trace
  use mpi_f08
  implicit none

  private

  type, public :: comms_distribution
    type(MPI_Comm)        :: communicator
    character(len=16)     :: layout
    integer               :: dimensions
    integer, allocatable  :: iproc(:)
    integer, allocatable  :: nprocs(:)
    logical               :: on_root
    logical               :: active
    logical               :: initialised = .false.

  end type comms_distribution

  type(comms_distribution), public, protected  :: comm_world

  public :: comms_initialise, &
            comms_finalise, &
            comms_generate_distribution, &
            comms_barrier, &
            comms_send, &
            comms_recv, &
            comms_reduce, &
            comms_transpose, &
            comms_abort

  type :: transpose_data
    character(len=64)     :: label = " "
    integer, allocatable  :: send_lookup(:,:), send_sizes(:), send_offsets(:)
    integer, allocatable  :: recv_lookup(:,:), recv_sizes(:), recv_offsets(:)
  end type transpose_data

  type(transpose_data) :: T_lookup(4)

  integer, save :: comm_color = 0
contains

  !----------------------------------------------------------------------------#
  subroutine comms_initialise()
    integer :: ierr

    call trace_entry("comms_initialise")

    allocate(comm_world%iproc(0:1), stat=ierr)
    if (ierr /= 0) call comms_abort("Unable to allocate comm_world%iproc")
    allocate(comm_world%nprocs(0:1), stat=ierr)
    if (ierr /= 0) call comms_abort("Unable to allocate comm_world%nprocs")

    comm_world%communicator = MPI_COMM_WORLD

    call MPI_Init(ierr)

    call MPI_Comm_size(comm_world%communicator, comm_world%nprocs(1), ierr)
    call MPI_Comm_rank(comm_world%communicator, comm_world%iproc(1), ierr)
    comm_world%nprocs(0) = comm_world%nprocs(1)
    comm_world%iproc = comm_world%iproc(1)+1


    comm_world%dimensions = 1
    comm_world%on_root    = (all(comm_world%iproc == 1))

    comm_world%layout = "global"

    comm_world%initialised = .true.

    call trace_exit("comms_initialise")
  end subroutine comms_initialise

  !----------------------------------------------------------------------------#
  subroutine comms_finalise()
    integer :: ierr

    call trace_entry("comms_finalise")

    deallocate(comm_world%iproc, comm_world%nprocs)
    comm_world%on_root      = .false.
    comm_world%initialised  = .false.

    call MPI_Finalize(ierr)

    call trace_exit("comms_finalise")
  end subroutine comms_finalise

  !----------------------------------------------------------------------------#
  subroutine comms_abort(str)
    use, intrinsic :: iso_fortran_env, only: error_unit
    character(len=*), intent(in) :: str

    if (comm_world%on_root) then
      write(error_unit,*) str
      call trace_write_callstack(error_unit)
    end if
    error stop 1

  end subroutine comms_abort

  !----------------------------------------------------------------------------#
  subroutine comms_generate_distribution(comm, layout, sizes)
    type(comms_distribution), intent(inout) :: comm
    character(len=*),         intent(in)    :: layout
    integer,                  intent(in)    :: sizes(:)

    integer,  allocatable :: tmp_sizes(:)
    integer :: distribution_sizes(2), factors(2)
    integer :: active_nprocs, tmp_nprocs, max_ndims
    integer :: dim_offset, factor
    integer :: str_loc
    integer :: ierr

    call trace_entry("comms_generate_distribution")

    ! Parse the `layout` string to get the number of posssibly distributed dimensions
    str_loc = index(layout, ';')
    max_ndims = 0

    do while (str_loc /= 0)
      max_ndims = max_ndims + 1
      if (index(layout(str_loc+1:), ',') == 0) exit
      str_loc = str_loc + index(layout(str_loc+1:), ',')
    end do

    if (max_ndims > size(sizes,1)) &
      call comms_abort("Attempting to distribute more dimensions than exist")


    ! Copy in the distributed dimension sizes into the `tmp_sizes` array
    allocate(tmp_sizes(max_ndims), stat=ierr)
    if (ierr /= 0) call comms_abort("Unable to allocate tmp_sizes")

    do dim_offset = 0, max_ndims-1
      tmp_sizes(max_ndims - dim_offset) = sizes(size(sizes,1) - dim_offset)
    end do

    ! Decrease the number of processes until we find a value commensurate with the grid
    proc_loop: do tmp_nprocs = comm_world%nprocs(1), 0, -1
      if (tmp_nprocs == 0) error stop 2

      ! Loop over factors of this number of processes
      factor_loop: do factor = 1, tmp_nprocs
        factors(1) = factor
        factors(2) = tmp_nprocs / factors(1)
        if (mod(tmp_nprocs, factors(1)) /= 0) cycle
        if (factors(1) > tmp_nprocs) exit factor_loop

        ! If this set of factors is commensurate with our array sizes, use it
        if ( mod(tmp_sizes(2), factors(2)) == 0 .and. &
             mod(tmp_sizes(1), factors(1)) == 0 ) then
          distribution_sizes(:) = factors(:)
          active_nprocs = tmp_nprocs
          exit proc_loop
        end if
      end do factor_loop
    end do proc_loop

    ! Create the comms_distribution type for this distribtion
    allocate(comm%iproc(0:2), comm%nprocs(0:2))
    comm%dimensions = 2
    comm%nprocs(1:2)  = distribution_sizes
    comm%nprocs(0) = product(distribution_sizes)

    comm%active  = (comm_world%iproc(1) <= active_nprocs)
    comm%layout  = layout
    comm%initialised = .true.

    ! Create an MPI communicator containing only the active processes
    if (comm%active) then
      call MPI_Comm_split(MPI_COMM_WORLD, comm_color, comm_world%iproc(1), comm%communicator, ierr)
      if (ierr /= 0) call comms_abort("Error splitting comm on active process")
      comm%iproc   = [  comm_world%iproc(1),                        &
                        mod(comm_world%iproc(1)-1, comm%nprocs(1))+1,  &
                        (comm_world%iproc(1)-1) / comm%nprocs(1)+1     &
                     ]
      comm%on_root = all(comm%iproc == 1)
    else 
      call MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, comm_world%iproc(1), comm%communicator, ierr)
      if (ierr /= 0) call comms_abort("Error splitting comm on inactive process")
      comm%iproc   = [-1, -1, -1]
      comm%on_root = .false.
    end if
    comm_color=comm_color+1

    deallocate(tmp_sizes)
    call trace_exit("comms_generate_distribution")
  end subroutine comms_generate_distribution

  subroutine comms_barrier(comm)
    type(comms_distribution), intent(in) :: comm
    integer :: ierr

    call trace_entry('comms_barrier')

    call MPI_Barrier(comm%communicator, ierr)
    if (ierr /= 0) call comms_abort("Failed executing MPI_Barrier")

    call trace_exit('comms_barrier')
  end subroutine comms_barrier

  subroutine comms_send(val, dest, comm, tag)
    class(*),                 intent(in)           :: val
    integer,                  intent(in)           :: dest
    type(comms_distribution), intent(in), optional :: comm
    integer,                  intent(in), optional :: tag

    type(MPI_Comm)  :: local_comm
    integer         :: local_tag
    integer         :: ierr

    call trace_entry('comms_send')

    local_comm = comm_world%communicator
    if (present(comm)) local_comm = comm%communicator

    local_tag = MPI_ANY_TAG
    if (present(tag)) local_tag = tag

    call MPI_Send(val, 1, comms_get_datatype(val), dest-1, local_tag, local_comm, ierr)
    if (ierr /= 0) call comms_abort("Failed executing MPI_Send")

    call trace_exit('comms_send')
  end subroutine comms_send

  subroutine comms_recv(val, source, comm, tag)
    class(*),                 intent(inout)        :: val
    integer,                  intent(in), optional :: source
    type(comms_distribution), intent(in), optional :: comm
    integer,                  intent(in), optional :: tag

    integer           :: local_source
    type(MPI_Comm)    :: local_comm
    integer           :: local_tag
    type(MPI_Status)  :: stat
    integer           :: ierr

    call trace_entry('comms_recv')

    local_source = MPI_ANY_SOURCE
    if (present(source)) local_source = source-1

    local_comm = comm_world%communicator
    if (present(comm)) local_comm = comm%communicator

    local_tag = MPI_ANY_TAG
    if (present(tag)) local_tag = tag

    call MPI_Recv(val, 1, comms_get_datatype(val), local_source, local_tag, local_comm, stat, ierr)
    if (ierr /= 0) call comms_abort("Failed executing MPI_Recv")

    call trace_exit('comms_recv')
  end subroutine comms_recv

  subroutine  comms_reduce(val, distribution, op_str)
    class(*),                 intent(inout) :: val
    type(comms_distribution), intent(in)    :: distribution
    character(len=3),         intent(in)    :: op_str

    type(mpi_op)       :: op
    integer :: ierr

    call trace_entry("comms_reduce")

    select case (op_str)
      case ("sum", "SUM")
        op = MPI_SUM
      case ("max", "MAX")
        op = MPI_MAX
      case default
        call comms_abort("Invalid reduction operation" // op_str)
    end select

    call MPI_Allreduce(MPI_IN_PLACE, val, 1, comms_get_datatype(val), op, &
                    distribution%communicator, ierr)

    if (ierr /= 0) &
      call comms_abort("MPI_Reduce failed")

    call trace_exit("comms_reduce")
  end subroutine  comms_reduce

  subroutine  comms_transpose(A, A_distribution, A_T, A_T_distribution, A_bounds)
    complex(kind=dp), allocatable, intent(in)    :: A(:,:,:,:)
    type(comms_distribution),      intent(in)    :: A_distribution
    complex(kind=dp), allocatable, intent(inout) :: A_T(:,:,:,:)
    type(comms_distribution),      intent(in)    :: A_T_distribution
    integer,                       intent(in)    :: A_bounds(4)

    complex(kind=dp), allocatable :: send_buff(:), recv_buff(:)
    integer :: i, j, k, l, ibuf, ilook, T_id
    integer :: nsends, nrecvs
    integer :: iproc, ierr


    call trace_entry("comms_transpose")

    ! Work out how much data we're sending to/from each process. This will be either nsends/nrecvs or 0
    nsends = 0
    nrecvs = 0
    if (A_distribution%active)   nsends = product(A_bounds) / A_distribution%nprocs(0)
    if (A_T_distribution%active) nrecvs = product(A_bounds) / A_T_distribution%nprocs(0)

    ! Get the lookup table, sizes and offsets for this transpose.
    ! If they're not cached, compute and store them
    call trace_entry("comms_transpose:cache_lookups")
    T_id = 0
    do ilook=1, size(T_lookup,1)
      if (T_lookup(ilook)%label == trim(A_distribution%layout)//"+"//trim(A_T_distribution%layout)) then
        T_id = ilook
        exit

      else if (T_lookup(ilook)%label == " ") then
        ! Name this transpose cache
        T_lookup(ilook)%label = trim(A_distribution%layout)//"+"//trim(A_T_distribution%layout)

        ! Allocate the arrays needed for the lookup
        allocate( T_lookup(ilook)%send_lookup(nsends,4),  &
                  T_lookup(ilook)%send_sizes(comm_world%nprocs(1)),   &
                  T_lookup(ilook)%send_offsets(comm_world%nprocs(1)), &
                  T_lookup(ilook)%recv_lookup(nrecvs,4),  &
                  T_lookup(ilook)%recv_sizes(comm_world%nprocs(1)),   &
                  T_lookup(ilook)%recv_offsets(comm_world%nprocs(1)) )

        ! Figure out the order we'll be copying data into the send buffer
        ! NB. This is a bit brute forced but it also computes the send sizes
        ibuf = 1
        T_lookup(ilook)%send_sizes = 0
        do iproc = 1, A_T_distribution%nprocs(0)
          do j=1, A_bounds(2)
            do i=1, A_bounds(1)
              if ( is_on_process([i,j], iproc, A_bounds(1:2), A_T_distribution) ) then
                do l=1, A_bounds(4)
                  do k=1, A_bounds(3)
                    if ( is_on_process([k,l], A_distribution%iproc(0), A_bounds(3:4),   A_distribution) ) then
                      T_lookup(ilook)%send_lookup(ibuf,:) = [i,j,k,l]
                      T_lookup(ilook)%send_sizes(iproc)=T_lookup(ilook)%send_sizes(iproc)+1
                      ibuf=ibuf+1
                    end if
                  end do
                end do
              end if
            end do
          end do
        end do

        ! Calculate the send offsets for this transpose
        T_lookup(ilook)%send_offsets = 0
        do i = 2, A_T_distribution%nprocs(0)
          T_lookup(ilook)%send_offsets(i) = T_lookup(ilook)%send_offsets(i-1) + T_lookup(ilook)%send_sizes(i-1)
        end do

        ! Figure out how much data we're receiving from each process, and what the offsets to those data are
        if (comm_world%iproc(1) <= A_T_distribution%nprocs(0)) then
          T_lookup(ilook)%recv_sizes(1:A_distribution%nprocs(0))  = product(A_bounds) &
                                                                  / (A_T_distribution%nprocs(0)*A_distribution%nprocs(0))
          T_lookup(ilook)%recv_sizes(A_distribution%nprocs(0)+1:) = 0

          T_lookup(ilook)%recv_offsets = 0
          do i = 2, A_distribution%nprocs(0)
            T_lookup(ilook)%recv_offsets(i) = T_lookup(ilook)%recv_offsets(i-1) + T_lookup(ilook)%recv_sizes(i-1)
          end do
        else
          T_lookup(ilook)%recv_sizes = 0
          T_lookup(ilook)%recv_offsets = 0
        end if


        ! Figure out the order we'll be copying data out the receive buffer
        ibuf = 1
        do iproc = 1, A_distribution%nprocs(0)
          do j=1, A_bounds(2)
            do i=1, A_bounds(1)
              if ( is_on_process([i,j], A_T_distribution%iproc(0), A_bounds(1:2), A_T_distribution)) then
                do l=1, A_bounds(4)
                  do k=1, A_bounds(3)
                    if ( is_on_process([k,l], iproc, A_bounds(3:4),   A_distribution) ) then
                      T_lookup(ilook)%recv_lookup(ibuf,:) = [k,l,i,j]
                      ibuf=ibuf+1
                    end if
                  end do
                end do
              end if
            end do
          end do
        end do


        T_id = ilook
        exit

      end if
    end do
    if (T_id == 0) call comms_abort("Too many transpose_ids")
    call trace_exit("comms_transpose:cache_lookups")


    ! Allocate the arrays needed for MPI_Alltoallv
    allocate(send_buff(nsends), recv_buff(nrecvs))

    ! Copy the data from A into the send buffer in the right order
    ! NB. This is a bit brute forced but it also computes the send sizes, and (I suspect) works
    call trace_entry("comms_transpose:copyin")
    do i=1, nsends
      send_buff(i) = A(T_lookup(T_id)%send_lookup(i,1), &
                      T_lookup(T_id)%send_lookup(i,2), &
                      T_lookup(T_id)%send_lookup(i,3), &
                      T_lookup(T_id)%send_lookup(i,4))
    end do
    call trace_exit("comms_transpose:copyin")

    call trace_entry("comms_transpose:alltoallv")
    ! Call MPI_Alltoallv to get the right data on the right processes
    call MPI_Alltoallv(send_buff, T_lookup(T_id)%send_sizes, T_lookup(T_id)%send_offsets, MPI_DOUBLE_COMPLEX, &
                       recv_buff, T_lookup(T_id)%recv_sizes, T_lookup(T_id)%recv_offsets, MPI_DOUBLE_COMPLEX, &
                       MPI_COMM_WORLD, ierr)
    call trace_exit("comms_transpose:alltoallv")

    ! Copy the data from the receive buffer into A_T in the right order
    call trace_entry("comms_transpose:copyout")
    do i=1, nrecvs
      A_T(T_lookup(T_id)%recv_lookup(i,1), &
          T_lookup(T_id)%recv_lookup(i,2), &
          T_lookup(T_id)%recv_lookup(i,3), &
          T_lookup(T_id)%recv_lookup(i,4)) = recv_buff(i)
    end do
    call trace_exit("comms_transpose:copyout")

    deallocate(send_buff, recv_buff)

    call trace_exit("comms_transpose")
  contains

    function is_on_process(indices, iproc, M_bounds, dist)
      logical :: is_on_process
      integer,                        intent(in) :: indices(2)
      integer,                        intent(in) :: iproc
      integer,                        intent(in) :: M_bounds(2)
      type(comms_distribution),       intent(in) :: dist

      integer :: iproc_range(2,2)

      is_on_process = .false.

      iproc_range(1,1) = ( mod((iproc-1),dist%nprocs(1))   ) * (M_bounds(1)/dist%nprocs(1))+1
      iproc_range(2,1) = ( mod((iproc-1),dist%nprocs(1))+1 ) * (M_bounds(1)/dist%nprocs(1))
      iproc_range(1,2) = ( (iproc-1) / dist%nprocs(1)    )   * (M_bounds(2)/dist%nprocs(2))+1
      iproc_range(2,2) = ( (iproc-1) / dist%nprocs(1)+1  )   * (M_bounds(2)/dist%nprocs(2))

      if (indices(1) >= iproc_range(1,1) .and. indices(1) <= iproc_range(2,1) .and. &
          indices(2) >= iproc_range(1,2) .and. indices(2) <= iproc_range(2,2) ) then
        is_on_process = .true.
      end if

    end function is_on_process

  end subroutine  comms_transpose

  function comms_get_datatype(val) result(datatype)
    type(MPI_Datatype) :: datatype
    class(*), intent(in) :: val

    select type(val)
      type is (integer);          datatype = MPI_INTEGER
      type is (complex(kind=dp)); datatype = MPI_DOUBLE_COMPLEX
      type is (real(kind=dp));    datatype = MPI_DOUBLE_PRECISION
      type is (logical);          datatype = MPI_LOGICAL
    end select
  end function

end module communications
