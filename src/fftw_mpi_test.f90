program fftw_mpi_test
!==============================================================================#
! FFTW_MPI_TEST
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-02-14
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use, intrinsic :: iso_c_binding
  use mpi
  implicit none
  include 'fftw3-mpi.f03'

  real(c_double),      parameter :: pi = 3.14159265358979d0
  integer(c_intptr_t), parameter :: L = 1000
  integer(c_intptr_t), parameter :: M = 1000
  integer(c_intptr_t), parameter :: N = 2
  integer :: icount
  type(C_PTR) :: plan, cdata, cdata2
  complex(C_DOUBLE_COMPLEX), pointer :: fdata(:,:,:), fdata2(:,:,:)
  complex(C_DOUBLE_COMPLEX) :: data_sum
  integer(C_INTPTR_T) :: i, j, alloc_local, local_m, local_j_offset
  integer :: iproc, ierr

  call MPI_Init(ierr)
  call MPI_Comm_rank(MPI_COMM_WORLD, iproc, ierr)

  call fftw_mpi_init()

  ! Get local data size and allocate (note dimension reversal)
  alloc_local = fftw_mpi_local_size_many(2, [M, L], N, FFTW_MPI_DEFAULT_BLOCK, MPI_COMM_WORLD, local_M, local_j_offset)

  cdata = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata, fdata, [N, L,local_M])

  cdata2 = fftw_alloc_complex(alloc_local)
  call c_f_pointer(cdata2, fdata2, [N, L,local_M])

  ! Create MPI plan for in-place forward DFT (note dimension reversal)
  plan = fftw_mpi_plan_many_dft(2, [M, L], N, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK, &
                                    fdata, fdata2, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE)

  fdata = (0.0d0, 0.0d0)
  ! Initiaalize data to some function
  do j=1, local_M
    do i = 1, L
      fdata(:, i, j) = sin(4*i*pi/M) * cos(2*(j+local_j_offset)*pi/L)
    end do
  end do

  print *, "Process", iproc, "holds", local_M, "rows starting at row", local_j_offset
  ! Compute transform (sa many times as desired)
  do icount = 1, 1
    call fftw_mpi_execute_dft(plan, fdata, fdata2)
  end do

  ! Sum the data and print out the answer on all cores
  do i=1, N
    data_sum = sum(fdata2(i,:,:))
    call MPI_Allreduce(MPI_IN_PLACE, data_sum, 1, MPI_DOUBLE_COMPLEX, MPI_SUM, &
                    MPI_COMM_WORLD, ierr)

    print *, iproc, i, data_sum
  end do

  call fftw_destroy_plan(plan)
  call fftw_free(cdata)

  call MPI_Finalize(ierr)

end program fftw_mpi_test
