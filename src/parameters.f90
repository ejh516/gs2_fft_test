module parameters
!==============================================================================#
! PARAMETERS
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-26
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use trace
  use regex
  use communications
  implicit none

  private

  type, public :: params_type
    character(len=32) :: job_name = "gs2_test"
    integer           :: ntheta = 10
    integer           :: nsigma = 2
    integer           :: grid_size = 128
    integer           :: niters = 10
    character(len=16) :: k_distribution = "noncontiguous"
  end type params_type

  type(params_type), public, protected :: params

  public :: params_initialise

contains

  subroutine params_initialise(args)
    character(len=64), intent(in) :: args(:)
    character(len=64), allocatable :: pair(:)
    integer :: i

    call trace_entry("params_initialse")

    do i = 1, size(args)
      call re_split("=", args(i), pair)
      if (size(pair) /= 2) call comms_abort("Command line arguments can only come in 'thing=val' pairs")

      select case(trim(pair(1)))
        case ("job_name")
          read(pair(2),*) params%job_name
        case ("ntheta")
          read(pair(2),*) params%ntheta
        case ("nsigma")
          read(pair(2),*) params%nsigma
        case ("grid_size")
          read(pair(2),*) params%grid_size
        case ("niters")
          read(pair(2),*) params%niters
        case ("k_distribution")
          read(pair(2),*) params%k_distribution
        case default
          call comms_abort("Invalid parameter: "//trim(pair(1)))
      end select
    end do

    call trace_exit("params_initialse")
  end subroutine params_initialise

end module parameters
