program arrays_test
!==============================================================================#
! ARRAYS_TEST
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-01-27
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use constants
  use trace
  use arrays
  use communications
  use ISO_FORTRAN_ENV, only: output_unit
  implicit none


  type(complex_4d_array) :: A

  integer :: sizes(4)
  character(len=32) :: layout

  complex(kind=dp) :: A_sum
  integer :: i, j, k, l
  integer :: ierr



  call trace_initialise(enable_backtrace = .true., enable_timings = .true.)
  call comms_initialise()

  sizes  = [10, 10, 8, 10]
  layout ="x,y;theta,phi"
  call array_create(A, sizes, layout)

  if (comm_world%on_root) then
    print *, "Global size:  ", sizes
    print *, "Global layout:", layout
  end if

  do l = A%local_start(4), A%local_end(4)
    do k = A%local_start(3), A%local_end(3)
      do j = A%local_start(2), A%local_end(2)
        do i = A%local_start(1), A%local_end(1)
          A%elements(i,j,k,l) = ((l-1)*A%global_size(3) + k)**2
        end do
      end do
    end do
  end do


  A_sum = array_sum(A)
  if (A%distribution%on_root) then
    print *, "Array stored on", product(A%distribution%nprocs), "processes"
    print *, "Distribution is", A%distribution%nprocs
    print *, "Array sum =", A_sum
  end if

  do l = A%global_start(4), A%global_end(4)
    do k = A%global_start(3), A%global_end(3)
      if (l >= A%local_start(4) .and. l <= A%local_end(4) .and. &
          k >= A%local_start(3) .and. k <= A%local_end(3)) then
        write(*,'(a,I3,I5)', advance="no") ' |',comm_world%iproc(1), nint(real(A%elements(1,1,k,l)))
        flush(output_unit)
      end if
      call MPI_Barrier(comm_world%communicator, ierr)
      call sleep(1)
    end do
    if (comm_world%on_root) write(*,*) "|"
    call MPI_Barrier(comm_world%communicator, ierr)
  end do

  call comms_finalise()

end program arrays_test
