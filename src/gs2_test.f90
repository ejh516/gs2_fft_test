program gs2_test
!==============================================================================#
! GS2_TEST
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-26
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use constants
  use trace
  use parameters
  use communications
  use arrays
  use ffts
  implicit none

  type(complex_4D_array) :: G
  type(complex_4D_array) :: del_sq_G
  type(fft_plan_type)    :: fft_plans(2)
  real(kind=dp) :: E
  integer :: iter

  character(len=64), allocatable :: args(:)
  integer :: iarg, nargs

  real(kind=dp) :: fft_time
  integer :: fft_calls

  ! Initialise the calculation
  call trace_initialise(enable_timings = .true., verbosity=0)
  call comms_initialise()
  call trace_set_iproc(comm_world%iproc(1))

  ! Find out if any default parameters were overwritten in the command line
  ! arguments
  nargs = command_argument_count()
  allocate(args(nargs))
  do iarg = 1, nargs
    call get_command_argument(iarg, args(iarg))
  end do
  call params_initialise(args)

  ! Print out the parameters used this run
  if (comm_world%iproc(1) == 1) then
    write(*,*) "PARAMETERS"
    write(*,*) "Job name       =", params%job_name
    write(*,*) "ntheta         =", params%ntheta
    write(*,*) "nsigma         =", params%nsigma
    write(*,*) "grid_size      =", params%grid_size
    write(*,*) "niters         =", params%niters
    write(*,*) "k_distribution = ", params%k_distribution
    write(*,*)" "
  end if

  call initialise_arrays(G, del_sq_G)

  ! Make plans for the FFTs we'll be doing this calculation
  call plan_ffts(G, del_sq_G, fft_plans)

  ! Repeatedly calculate E = -1/2 sum( del^2 G )
  do iter = 1, params%niters
    call apply_laplacian(G, del_sq_G, fft_plans)
    call calculate_E(del_sq_G, E)
  end do

  ! Report the result to check accuracy
  if (comm_world%on_root) print *, "Total energy: ", E

  ! Report the time spent doing the forward transform
  if (comm_world%on_root) then
    call trace_get_time("fft_xy_global", fft_time, fft_calls)
    print *, "Average FFT time over", fft_calls, "calls is", fft_time/fft_calls
  end if

  ! Clean up the calculation and report routine timings
  call cleanup_arrays(G, del_sq_G)
  call trace_output(trim(params%job_name) // ".trace")
  call comms_finalise()
  call trace_finalise()

contains

  ! Allocate G,del_sq_G and initialise G = cos(2 pi x/theta) + i sin(2 pi y/sigma)
  subroutine initialise_arrays(G, del_sq_G)
    type(complex_4d_array),   intent(inout)  :: G
    type(complex_4d_array),   intent(inout)  :: del_sq_G
    integer :: itheta, isigma, ix, iy

    call trace_entry("initialise_arrays")

    ! Allocate G and initialise to the appropriate values
    call array_create(G, [params%ntheta,params%nsigma,params%grid_size,params%grid_size], &
                      "theta,sigma;x,y")

    do iy = G%local_start(4), G%local_end(4)
      do ix = G%local_start(3), G%local_end(3)
        do isigma = G%global_start(2), G%global_end(2)
          do itheta = G%global_start(1), G%global_end(1)
            G%elements(itheta, isigma, ix, iy) = cos(2*pi*ix/itheta) + z_i*sin(2*pi*iy/isigma)
          end do
        end do
      end do
    end do

    ! Allocate del_sq_G and zero it
    call array_create(del_sq_G, [params%ntheta,params%nsigma,params%grid_size,params%grid_size], &
                      "theta,sigma;x,y")
    if (del_sq_G%on_this_node) del_sq_G%elements = (0.0_dp, 0.0_dp)

    call trace_exit("initialise_arrays")
  end subroutine initialise_arrays

  ! Plan the FFTs needed for this calculation
  subroutine plan_ffts(G, del_sq_G, plans)
    type(complex_4D_array), intent(inout)  :: G
    type(complex_4D_array), intent(inout)  :: del_sq_G
    type(fft_plan_type),    intent(out)    :: plans(2)

    type(complex_4D_array)  :: G_k

    call trace_entry("plan_ffts")

    call fft_init()

    ! Allocate G_k based on which parallel fourier distribution we're using
    if (params%k_distribution == "transposed") then
      call array_create(G_k, [params%grid_size,params%grid_size,params%ntheta,params%nsigma], &
                        "kx,ky;theta,sigma")

    else if (params%k_distribution == "noncontiguous") then
      call array_create(G_k, [params%ntheta,params%nsigma,params%grid_size,params%grid_size], &
                        "theta,sigma;kx,ky")
    end if

    ! Plan the FFTs we'll be doing
    call fft_plan_xy_global(G, G_k, plans(1))
    call fft_plan_inverse_xy_global(G_k, del_sq_G, plans(2))

    call array_destroy(G_k)

    call trace_exit("plan_ffts")
  end subroutine plan_ffts

  ! Calculate del^2 G for a given G
  subroutine apply_laplacian(G, del_sq_G, plans)
    type(complex_4d_array),   intent(inout) :: G
    type(complex_4d_array),   intent(inout) :: del_sq_G
    type(fft_plan_type),      intent(in)    :: plans(2)

    type(complex_4D_array)  :: G_k
    integer :: kx, ky, itheta, isigma

    call trace_entry("apply_laplacian")

    ! Allocate G_k based on which parallel fourier distribution we're using
    if (params%k_distribution == "transposed") then
      call array_create(G_k, [params%grid_size,params%grid_size,params%ntheta,params%nsigma], &
                        "kx,ky;theta,sigma")

    else if (params%k_distribution == "noncontiguous") then
      call array_create(G_k, [params%ntheta,params%nsigma,params%grid_size,params%grid_size], &
                        "theta,sigma;kx,ky")
    end if

    ! Fourier transform G to get G_k
    call fft_xy_global(G, G_k, plans(1))

    ! Calculate del^2 G_k = k^2 * G_k
    if (G_k%layout == "theta,sigma;kx,ky") then ! The data's not been transposed during the FFT
      do ky = G_k%local_start(4), G_k%local_end(4)
        do kx = G_k%local_start(3), G_k%local_end(3)
          do isigma = G_k%global_start(2), G_k%global_end(2)
            do itheta = G_k%global_start(1), G_k%global_end(1)
              G_k%elements(itheta,isigma,kx,ky) = -(kx**2+ky**2)*G_k%elements(itheta,isigma,kx,ky)
            end do
          end do
        end do
      end do

    else if (G_k%layout == "kx,ky;theta,sigma") then ! The data's been transposed during the FFT
      do isigma = G_k%local_start(4), G_k%local_end(4)
        do itheta = G_k%local_start(3), G_k%local_end(3)
          do ky = G_k%global_start(2), G_k%global_end(2)
            do kx = G_k%global_start(1), G_k%global_end(1)
              G_k%elements(kx,ky,itheta,isigma) = -(kx**2+ky**2)*G_k%elements(kx,ky,itheta,isigma)
            end do
          end do
        end do
      end do
    end if

    ! Transform del^2 G back into realspace
    call fft_inverse_xy_global(G_k, del_sq_G, plans(2))

    call array_destroy(G_k)

    call trace_exit("apply_laplacian")
  end subroutine apply_laplacian

  ! Calculate E = 1/2 * del_sq_G
  subroutine calculate_E(del_sq_G, E)
    type(complex_4d_array),   intent(in)  :: del_sq_G
    real(kind=dp),            intent(out) :: E

    call trace_entry("calculate_E")

    E = 0.5_dp * real(array_sum(del_sq_G), dp)

    call trace_exit("calculate_E")
  end subroutine calculate_E

  ! deallocate local arrays
  subroutine cleanup_arrays(G, del_sq_G)
    type(complex_4d_array),   intent(inout) :: G
    type(complex_4d_array),   intent(inout) :: del_sq_G

    call trace_entry("cleanup_arrays")

    call array_destroy(G)
    call array_destroy(del_sq_G)

    call trace_exit("cleanup_arrays")
  end subroutine cleanup_arrays

end program gs2_test
