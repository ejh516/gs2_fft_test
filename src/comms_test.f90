program comms_test
!==============================================================================#
! COMMS_TEST
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-01-21
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use communications
  use trace
  use mpi_f08
  implicit none

  type(comms_distribution) :: comm
  integer :: i, ierr


  call trace_initialise(enable_backtrace = .true., enable_timings = .true.)
  call comms_initialise()

  call comms_generate_distribution(comm, "a,b;c,d", [1, 1, 32, 100])

  do i=1, comm_world%nprocs(1)
    if (i == comm_world%iproc(1)) then
      print *, "Process", i, ":"
      print *, "  initialised  = ", comm%initialised
      print *, "  active       = ", comm%active
      print *, "  nprocs       = ", comm%nprocs
      print *, "  iproc        = ", comm%iproc
      print *, "  on_root      = ", comm%on_root
      print *, " "
    end if
    call MPI_Barrier(comm_world%communicator, ierr)
  end do

  call comms_finalise()

end program comms_test
