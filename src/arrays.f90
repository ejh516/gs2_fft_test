module arrays
!==============================================================================#
! ARRAYS
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-28
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use constants
  use trace
  use communications
  implicit none

  private

  type, public :: complex_4d_array
    complex(kind=dp), dimension(:,:,:,:), allocatable :: elements
    character(len=64)                                 :: layout
    integer                                           :: local_start(4)  = [1, 1, 1, 1]
    integer                                           :: local_end(4)    = [0, 0, 0, 0]
    integer                                           :: local_size(4)   = [0, 0, 0, 0]
    integer                                           :: global_start(4) = [1, 1, 1, 1]
    integer                                           :: global_end(4)   = [0, 0, 0, 0]
    integer                                           :: global_size(4)  = [0, 0, 0, 0]
    logical                                           :: is_allocated = .false.
    logical                                           :: on_this_node = .false.
    type(comms_distribution)                          :: distribution
  end type complex_4d_array

  public :: array_create,           &
            array_destroy,          &
            array_sum,              &
            array_transpose

contains

    subroutine array_create(array, sizes, layout)
      type(complex_4d_array), intent(inout) :: array
      integer,                intent(in)    :: sizes(4)
      character(len=*),       intent(in)    :: layout

      call trace_entry("array_create")

      if (array%is_allocated) call comms_abort("ERROR: Trying to allocate and array that is already allocated")

      array%global_start = [1,1,1,1]
      array%global_end = sizes
      array%global_size = sizes

      call comms_generate_distribution(array%distribution, layout, sizes)

      if (array%distribution%active) then
        array%local_start(1:2) = [1,1]
        array%local_end(1:2)   = sizes(1:2)
        array%local_size(1:2)  = sizes(1:2)

        array%local_size(3)  = sizes(3) / array%distribution%nprocs(1)
        array%local_start(3) = array%local_size(3) * (array%distribution%iproc(1)-1) + 1
        array%local_end(3)   = array%local_start(3) + array%local_size(3) - 1

        array%local_size(4)  = sizes(4) / array%distribution%nprocs(2)
        array%local_start(4) = array%local_size(4) * (array%distribution%iproc(2)-1) + 1
        array%local_end(4)   = array%local_start(4) + array%local_size(4) - 1

        allocate( array%elements( array%local_start(1) : array%local_end(1),  &
                                  array%local_start(2) : array%local_end(2),  &
                                  array%local_start(3) : array%local_end(3),  &
                                  array%local_start(4) : array%local_end(4) ) &
                )

        array%elements(:,:,:,:) = (0.0_dp, 0.0_dp)
        array%on_this_node = .true.

      else ! This process does not have any part of this matrix
        array%local_start  = [1,1,1,1]
        array%local_end    = [0,0,0,0]
        array%local_size   = [0,0,0,0]
        array%on_this_node = .false.
      end if

      array%is_allocated = .true.
      array%layout = layout

      call trace_exit("array_create")
    end subroutine array_create

    subroutine array_destroy(array)
      type(complex_4d_array), intent(inout) :: array
      integer :: ierr

      call trace_entry("array_destroy")

      if (.not. array%is_allocated) call comms_abort("ERROR: Trying to deallocate an array that is not allocated")

      if (array%on_this_node) then
        deallocate(array%elements, stat=ierr)
        if (ierr /= 0) call comms_abort("ERROR: Unable to deallocate array slice")

        array%local_start  = [1, 1, 1, 1]
        array%local_end    = [0, 0, 0, 0]
        array%local_size   = [0, 0, 0, 0]
        array%on_this_node = .false.
      end if

      array%layout       = ""
      array%global_start = [1, 1, 1, 1]
      array%global_end   = [0, 0, 0, 0]
      array%global_size  = [0, 0, 0, 0]
      array%is_allocated = .false.

      call trace_exit("array_destroy")
    end subroutine array_destroy

    function array_sum(array)
      complex(kind=dp)                    :: array_sum
      type(complex_4d_array), intent(in)  :: array

      call trace_entry("array_sum")

      array_sum  = (0.0_dp, 0.0_dp)
      if (array%on_this_node) then

        array_sum = sum(array%elements)

        if (array%distribution%dimensions > 0) then
          call comms_reduce(array_sum, array%distribution, "sum")
        end if
      end if

      call trace_exit("array_sum")
    end function array_sum

    subroutine array_transpose(A, A_T)
      type(complex_4d_array), intent(in)    :: A
      type(complex_4d_array), intent(inout) :: A_T
      integer :: i,j,k,l

      call trace_entry("array_transpose")

      if (A%distribution%dimensions == 0) then
        do j = A%global_start(2), A%global_end(2)
          do i = A%global_start(1), A%global_end(1)
            do l = A%global_start(4), A%global_end(4)
              do k = A%global_start(3), A%global_end(3)
                A_T%elements(k,l,i,j) = A%elements(i,j,k,l)
              end do
            end do
          end do
        end do

      else 
        call comms_transpose(A%elements, A%distribution, A_T%elements, A_T%distribution, A%global_size)
        !TODO: Figure out how to transpose the data locally once it's all on the right nodes
!EJH!         call comms_abort("SORRY: Array transposes on multiple processes is not yet supported")
      end if

      call trace_exit("array_transpose")
    end subroutine array_transpose

end module arrays
