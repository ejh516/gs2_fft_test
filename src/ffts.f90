module ffts
!==============================================================================#
! FFTS
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-28
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use trace
  use parameters
  use communications
  use arrays
  use, intrinsic :: iso_c_binding
  implicit none

  include 'fftw3-mpi.f03'

  private

  type, public :: fft_plan_type
    type(c_ptr) :: fftw_plan
    type(C_PTR) :: plan, cdata, cdata2
    integer(C_INTPTR_T) :: local_y_size, local_y_offset
    complex(C_DOUBLE_COMPLEX), pointer :: fdata(:,:,:), fdata2(:,:,:)
  end type fft_plan_type

  public :: fft_init,                                       &
            fft_plan_xy_global, fft_plan_inverse_xy_global, &
            fft_xy_global,      fft_inverse_xy_global

contains

  subroutine fft_init()

    call trace_entry("fft_init")

    if (params%k_distribution == "noncontiguous") then
      call fftw_mpi_init()
    end if

    call trace_exit("fft_init")
  end subroutine fft_init

  ! Generate a plan for the global xy FFT
  subroutine fft_plan_xy_global(Mx, Mk, plan)
    type(complex_4d_array), intent(inout)  :: Mx
    type(complex_4d_array), intent(inout)  :: Mk
    type(fft_plan_type),    intent(inout)  :: plan

    integer(C_INTPTR_T) :: c_grid_size, c_howmany, alloc_local

    call trace_entry("fft_plan_xy_global")

    ! If we're transposing the data, the FFT will be a standard fftw_dft_2d
    ! for each theta,sigma pair
    if (params%k_distribution == "transposed") then
      if (Mk%on_this_node) then
        call dfftw_plan_dft_2d( plan%fftw_plan,           &
                                params%grid_size,         &
                                params%grid_size,         &
                                Mk%elements,     &
                                Mk%elements,     &
                                FFTW_FORWARD, FFTW_MEASURE )
      endif

    else if (params%k_distribution == "noncontiguous") then
      ! If we're not transposing but all the data is local anyway, we can use
      ! fftw_many_dft API
      if (comm_world%nprocs(1) == 1) then
        call dfftw_plan_many_dft( plan%fftw_plan, 2, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, &
                                  Mx%elements, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, 1, &
                                  Mk%elements, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, 1, &
                                  FFTW_FORWARD, FFTW_MEASURE )

      ! Else we need the full fftw_mpi_many_dft behemouth
      else if (Mx%on_this_node) then

        ! Since fftw_mpi_plan_many_dft and fftw_mpi_execute_dft only take C-like
        ! arrays, we'll need to allocate some to use, and copy the Fortran data
        ! in when we want to call them
        c_grid_size = params%grid_size
        c_howmany   = params%nsigma * params%ntheta
        alloc_local = fftw_mpi_local_size_many( 2, [c_grid_size, c_grid_size], c_howmany, FFTW_MPI_DEFAULT_BLOCK, &
                                                Mx%distribution%communicator%MPI_VAL, plan%local_y_size, plan%local_y_offset)

        plan%cdata = fftw_alloc_complex(alloc_local)
        call c_f_pointer(plan%cdata, plan%fdata, [int(c_howmany), int(params%grid_size), int(plan%local_y_size)])
        plan%cdata2 = fftw_alloc_complex(alloc_local)
        call c_f_pointer(plan%cdata2, plan%fdata2, [int(c_howmany), int(params%grid_size), int(plan%local_y_size)])

        ! Now we can plan the distributed FFT
        plan%fftw_plan = fftw_mpi_plan_many_dft(2, [c_grid_size, c_grid_size],                                    &
                                                c_howmany, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,        &
                                                plan%fdata, plan%fdata2,                                          &
                                                Mx%distribution%communicator%MPI_VAL, FFTW_FORWARD, FFTW_MEASURE)
      end if
    end if

    call trace_exit("fft_plan_xy_global")
  end subroutine fft_plan_xy_global

  ! Generate a plan for the global inverse xy FFT
  subroutine fft_plan_inverse_xy_global(Mk, Mx, plan)
    type(complex_4d_array), intent(inout)  :: Mk
    type(complex_4d_array), intent(inout)  :: Mx
    type(fft_plan_type),    intent(inout)  :: plan

    integer(C_INTPTR_T) :: c_grid_size, c_howmany, alloc_local

    call trace_entry("fft_plan_inverse_xy_global")

    ! If we're transposing the data, the FFT will be a standard fftw_dft_2d
    ! for each theta,sigma pair
    if (params%k_distribution == "transposed") then
      if (Mk%on_this_node) then
        call dfftw_plan_dft_2d( plan%fftw_plan,           &
                                params%grid_size,         &
                                params%grid_size,         &
                                Mk%elements,     &
                                Mk%elements,     &
                                FFTW_BACKWARD, FFTW_MEASURE )
      end if

    else if (params%k_distribution == "noncontiguous") then
      ! If we're not transposing but all the data is local anyway, we can use
      ! fftw_many_dft API
      if (comm_world%nprocs(1) == 1) then
        call dfftw_plan_many_dft( plan%fftw_plan, 2, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, &
                                  Mk%elements, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, 1, &
                                  Mx%elements, &
                                  [params%grid_size, params%grid_size], params%nsigma*params%ntheta, 1, &
                                  FFTW_BACKWARD, FFTW_MEASURE )

      ! Else we need the full fftw_mpi_many_dft behemouth
      else if (Mk%on_this_node) then

        ! Since fftw_mpi_plan_many_dft and fftw_mpi_execute_dft only take C-like
        ! arrays, we'll need to allocate some to use, and copy the Fortran data
        ! in when we want to call them
        c_grid_size = params%grid_size
        c_howmany   = params%nsigma * params%ntheta
        alloc_local = fftw_mpi_local_size_many( 2, [c_grid_size, c_grid_size], c_howmany, FFTW_MPI_DEFAULT_BLOCK, &
                                                Mk%distribution%communicator%MPI_VAL, plan%local_y_size, plan%local_y_offset)

        plan%cdata = fftw_alloc_complex(alloc_local)
        call c_f_pointer(plan%cdata, plan%fdata, [int(c_howmany), int(params%grid_size), int(plan%local_y_size)])
        plan%cdata2 = fftw_alloc_complex(alloc_local)
        call c_f_pointer(plan%cdata2, plan%fdata2, [int(c_howmany), int(params%grid_size), int(plan%local_y_size)])

        ! Now we can plan the distributed FFT
        plan%fftw_plan = fftw_mpi_plan_many_dft(2, [c_grid_size, c_grid_size],                                    &
                                                c_howmany, FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,        &
                                                plan%fdata, plan%fdata2,                                          &
                                                Mk%distribution%communicator%MPI_VAL, FFTW_BACKWARD, FFTW_MEASURE)
      end if
    end if

    call trace_exit("fft_plan_inverse_xy_global")
  end subroutine fft_plan_inverse_xy_global

  subroutine fft_xy_global(Mx, Mk, plan)
    use, intrinsic :: ISO_FORTRAN_ENV, only: output_unit
    type(complex_4d_array), intent(inout) :: Mx
    type(complex_4d_array), intent(inout) :: Mk
    type(fft_plan_type),    intent(in)    :: plan

    type(complex_4d_array)  :: Mx_T
    integer :: ix, iy, isigma, itheta

    call trace_entry("fft_xy_global")

    ! If we're transposing the data, allocate the transpose and FFT each local
    ! theta,sigma pair
    if (params%k_distribution == "transposed") then

      ! Calculate the global transpose of Mx (theta,sigma;x,y -> x,y;theta,sigma)
      call array_create(Mx_T, [params%grid_size,params%grid_size,params%ntheta,params%nsigma], &
                        "x,y;theta,sigma")

      call array_transpose(Mx, Mx_T)

      ! FFT the local data
      do isigma = Mx_T%local_start(4), Mx_T%local_end(4)
        do itheta = Mx_T%local_start(3), Mx_T%local_end(3)
          call dfftw_execute_dft( plan%fftw_plan, Mx_T%elements(:,:,itheta,isigma), &
                                  Mk%elements(:,:,itheta, isigma) )
        end do
      end do

      call array_destroy(Mx_T)

    else if (params%k_distribution == "noncontiguous") then

      ! If the data's all local, execute the appropriate fftw plan
      if (comm_world%nprocs(1) == 1) then
        call dfftw_execute_dft(plan%fftw_plan, Mx%elements, Mk%elements)

      ! Otherwise do the distributed FFT
      else if (Mx%on_this_node) then

        ! Copy the Fortran data into the C-like buffer
        do iy = Mx%local_start(4), Mx%local_end(4)
          do ix = Mx%local_start(3), Mx%local_end(3)
            do isigma = Mx%local_start(2), Mx%local_end(2)
              do itheta = Mx%local_start(1), Mx%local_end(1)
                plan%fdata(itheta+(isigma-1)*Mx%local_size(1), ix, iy-plan%local_y_offset) = Mx%elements(itheta, isigma, ix, iy)
              end do
            end do
          end do
        end do

        ! Execute the FFT on the C-like data
        call fftw_mpi_execute_dft(plan%fftw_plan, plan%fdata, plan%fdata2)

        ! Copy the transformed data back into the right place
        do iy = Mk%local_start(4), Mk%local_end(4)
          do ix = Mk%local_start(3), Mk%local_end(3)
            do isigma = Mk%local_start(2), Mk%local_end(2)
              do itheta = Mk%local_start(1), Mk%local_end(1)
                Mk%elements(itheta, isigma, ix, iy) = plan%fdata2(itheta+(isigma-1)*Mx%local_size(1), ix, iy-plan%local_y_offset)
              end do
            end do
          end do
        end do

      end if
    end if

    call trace_exit("fft_xy_global")
  end subroutine fft_xy_global

  subroutine fft_inverse_xy_global(Mk, Mx, plan)
    type(complex_4d_array), intent(inout) :: Mk
    type(complex_4d_array), intent(inout) :: Mx
    type(fft_plan_type),    intent(in)    :: plan

    type(complex_4d_array)  :: Mx_T
    integer :: ix, iy, isigma, itheta

    call trace_entry("fft_inverse_xy_global")

    ! If we're transposing the data, allocate the transpose and FFT each local
    ! theta,sigma pair
    if (params%k_distribution == "transposed") then

      ! Allocate Mx_T for holding the transpose of Mx
      call array_create(Mx_T, [params%grid_size,params%grid_size,params%ntheta,params%nsigma], &
                        "x,y;theta,sigma")

      ! FFT the local data
      do isigma = Mx_T%local_start(4), Mx_T%local_end(4)
        do itheta = Mx_T%local_start(3), Mx_T%local_end(3)
          call dfftw_execute_dft( plan%fftw_plan, Mk%elements(:,:,itheta,isigma), &
                                  Mx_T%elements(:,:,itheta, isigma) )
        end do
      end do

      ! Transpose the data back to the nonlocal distribution (x,y;theta,sigma -> theta,sigma;x,y)
      call array_transpose(Mx_T, Mx)

      call array_destroy(Mx_T)

    else if (params%k_distribution == "noncontiguous") then

      ! If the data's all local, execute the appropriate fftw plan
      if (comm_world%nprocs(1) == 1) then
        call dfftw_execute_dft(plan%fftw_plan, Mk%elements, Mx%elements)

      ! Otherwise do the distributed FFT
      else if (Mk%on_this_node) then

        ! Copy the Fortran data into the C-like buffer
        do iy = Mk%local_start(4), Mk%local_end(4)
          do ix = Mk%local_start(3), Mk%local_end(3)
            do isigma = Mk%local_start(2), Mk%local_end(2)
              do itheta = Mk%local_start(1), Mk%local_end(1)
                plan%fdata(itheta+(isigma-1)*Mk%local_size(1), ix, iy-plan%local_y_offset) = Mk%elements(itheta, isigma, ix, iy)
              end do
            end do
          end do
        end do

        ! Execute the FFT on the C-like data
        call fftw_mpi_execute_dft(plan%fftw_plan, plan%fdata, plan%fdata2)

        ! Copy the transformed data back into the right place
        do iy = Mx%local_start(4), Mx%local_end(4)
          do ix = Mx%local_start(3), Mx%local_end(3)
            do isigma = Mx%local_start(2), Mx%local_end(2)
              do itheta = Mx%local_start(1), Mx%local_end(1)
                Mx%elements(itheta, isigma, ix, iy) = plan%fdata2(itheta+(isigma-1)*Mx%local_size(1), ix, iy-plan%local_y_offset)
              end do
            end do
          end do
        end do

      end if
    end if

    call trace_exit("fft_inverse_xy_global")
  end subroutine fft_inverse_xy_global

end module ffts
