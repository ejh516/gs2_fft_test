module constants
!==============================================================================#
! CONSTANTS
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2019-11-28
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  implicit none

  public

  ! Computational/mathematical constants
  integer, parameter          :: dp  = selected_real_kind(15,300)
  real(kind=dp), parameter    :: pi  = 3.141592653589793_dp
  complex(kind=dp), parameter :: z_i = (0, 1)


end module constants
