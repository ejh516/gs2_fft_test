!==============================================================================!
! TRACE                                                                        !
!==============================================================================!
! Module for timing routines in Aaron's pleb code                              !
!==============================================================================!
! Author: Edward Higgins, 2014-09-17                                           !
!==============================================================================!
module trace
  use constants
  use ISO_FORTRAN_ENV, only: error_unit
  implicit none

  private

  type routine
    character(len=64)       ::  id
    type(routine), pointer  ::  next
    real(kind=dp)           ::  start_time
    real(kind=dp)           ::  end_time
    real(kind=dp)           ::  total_time
    integer                 ::  num_calls
  end type routine

  ! Public subroutines
  public  ::  trace_entry,      &
              trace_exit,       &
              trace_initialise, &
              trace_finalise,   &
              trace_set_iproc,  &
              trace_output,     &
              trace_get_time,   &
              trace_write_callstack

  character(len=64),  public, save  ::  current_sub

  ! Module level variables
  type(routine), pointer, save  ::  times
  type(routine), pointer, save  ::  callstack
  logical,                save  ::  trace_enabled   = .true.
  logical,                save  ::  trace_timings   = .false.
  integer,                save  ::  trace_verbosity = 0
  integer,                save  ::  trace_iproc     = 1
  real(kind=dp),          save  ::  trace_time, trace_start

  integer :: depth

contains

  !------------------------------------------------------------------------------!
    subroutine trace_initialise(enable_backtrace, enable_timings, verbosity, &   !
                                iproc)                                           !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   A subroutine to initialise the timing and all stacks for the trace module  !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   logical,  opional,  intent(in)  ::  enable_backtrace                       !
  !     Whether or not the program backtrace is stored (default true)            !
  !                                                                              !
  !   logical,  opional,  intent(in)  ::  enable_backtrace                       !
  !     Whether or not the program timing information is stored (default false)  !
  !                                                                              !
  !   integer,  opional,  intent(in)  ::  verbosity                              !
  !     Level of output to print to stderr when entering/leaving a region        !
  !     (default 0)                                                              !
  !                                                                              !
  !   integer,  opional,  intent(in)  ::  iproc                                  !
  !     Process id for parallel applications                                     !
  !     (default 1)                                                              !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    implicit none

    logical,  intent(in), optional  ::  enable_backtrace
    logical,  intent(in), optional  ::  enable_timings
    integer,  intent(in), optional  ::  verbosity
    integer,  intent(in), optional  ::  iproc

    if(present(enable_backtrace)) trace_enabled   = enable_backtrace
    if(present(enable_timings))   trace_timings   = enable_timings
    if(present(verbosity))        trace_verbosity = verbosity
    if(present(iproc))            trace_iproc      = iproc

    if(.not. trace_enabled) return

    trace_start = trace_timer()

    ! Set up the callstack
    call routine_allocate(callstack, 'main')
    current_sub = 'main'


    ! Set up the times list
    if(trace_timings) then
      call routine_allocate(times, 'main')
      times%start_time = trace_timer()
    end if

    depth = 0

    ! Print any verbose information if required
    if (trace_verbosity == 0) then
      continue
    else if (trace_verbosity == 1 .and. trace_iproc == 1) then
      write(error_unit,*) "TRACE: Trace initialised"
      flush(error_unit)
    else if (trace_verbosity == 2) then
      write(error_unit,*) "TRACE: process ", trace_iproc, ": Trace initialised"
      flush(error_unit)
    end if

    trace_time = trace_timer() - trace_start

  end subroutine trace_initialise

  !------------------------------------------------------------------------------!
    subroutine trace_set_iproc(iproc)                                            !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   Sets the process id for this process once we know it (We usually don't     !
  !   know it during initialisation                                              !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   integer,  intent(in)  ::  iproc                                            !
  !     Routine being entered                                                    !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2020-02-07                                                 !
  !------------------------------------------------------------------------------!
    integer, intent(in) :: iproc

    trace_iproc = iproc

  end subroutine trace_set_iproc

  !------------------------------------------------------------------------------!
    subroutine trace_entry(sub_name, err)                                        !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   Signals the entry of a traced subroutine                                   !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   character,  intent(in)  ::  sub_name                                       !
  !     Routine being entered                                                    !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    use omp_lib

    implicit none

    character(len=*), intent(in)  ::  sub_name
    integer,          intent(inout),  optional  ::  err

    type(routine),  pointer       ::  current
    integer :: i

    if(.not. trace_enabled) return

!$  if(omp_get_thread_num() == 0) then
      trace_start = trace_timer()

      ! Add the current subroutine to the callstack
      current => callstack
      do
        if(associated(current%next)) then
          current => current%next
          cycle
        else
          call routine_allocate(current%next, sub_name)
          current => current%next
          current_sub = current%id
          exit
        end if
      end do

      ! Add the current subroutine to the times list and start the timer
      if(trace_timings) then
        current => times
        do
          if(current%id /= sub_name) then
            if(associated(current%next)) then
              current => current%next
              cycle
            else
              call routine_allocate(current%next, sub_name)
              current => current%next
              current%start_time = trace_timer()
              exit
            end if
          else if(current%end_time < 0.0_dp) then
            current%start_time = trace_timer()
            exit
          else
            write(*,*) "Error: trace_entry has already been called for " // trim(sub_name)
            stop
          end if
        end do
      end if

      ! Print any verbose information if required
      if (trace_verbosity == 0) then
        continue
      else if (trace_verbosity == 1 .and. trace_iproc == 1) then
        write(error_unit,'(A)',advance="no") "TRACE: "
        do i = 1, depth
          write(error_unit,'(a)',advance="no") "  "
        end do
        write(error_unit,'(A,A)') "Entering " // sub_name
        flush(error_unit)
        depth=depth+1
      else if (trace_verbosity == 2) then
        write(error_unit,*) "TRACE: process ", trace_iproc, ": Entering " // sub_name
        flush(error_unit)
      end if

      trace_time = trace_time + trace_timer() - trace_start
!$  end if

  end subroutine trace_entry

  !------------------------------------------------------------------------------!
    subroutine trace_exit(sub_name, err)                                              !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   Signals the exit of a traced subroutine                                    !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   character,  intent(in)  ::  sub_name                                       !
  !     Routine being exited                                                     !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    use omp_lib

    implicit none

    character(len=*), intent(in)  ::  sub_name
    integer,          intent(inout),  optional  ::  err

    type(routine),  pointer       ::  current
    integer :: i

    if(.not. trace_enabled) return

    trace_start = trace_timer()

!$  if(omp_get_thread_num() == 0) then

      ! Remove the current subroutine from the callstack
      if(associated(callstack%next)) then
        current => callstack
        do
          if(associated(current%next%next)) then
            current => current%next
            cycle
          else
            if(current%next%id == sub_name) then
              deallocate(current%next)
              current_sub = current%id
              exit
            else
              write(*,*) sub_name // " is not currently in the callstack, but trace_exit has been called."
              stop
            end if
          end if
        end do
      end if

      ! Finalise the subroutine timing in times
      if(trace_timings) then
        current => times
        do
          if(current%id /= sub_name) then
            if(associated(current%next)) then
              current => current%next
              cycle
            else
              write(*,*) "Error: trace_entry has not been called for " // trim(sub_name) // " but trace_exit has!"
              stop
            end if
          else if(current%start_time < 0.0_dp) then
            write(*,*) "Error: trace_entry has not been called for " // trim(sub_name) // " but trace_exit has!"
            stop
          else if(current%end_time > 0.0_dp) then
            write(*,*) "Error: trace_exit has not been reset for " // trim(sub_name)
            stop
          else
            current%end_time   = trace_start
            current%total_time = current%total_time + (current%end_time - current%start_time)
            current%start_time = -1.0_dp
            current%end_time   = -1.0_dp

            current%num_calls  = current%num_calls + 1
            exit
          end if
        end do
      end if

      ! Print any verbose information if required
      if (trace_verbosity == 0) then
        continue
      else if (trace_verbosity == 1 .and. trace_iproc == 1) then
        depth=depth-1
        write(error_unit,'(A)',advance="no") "TRACE: "
        do i = 1, depth
          write(error_unit,'(a)',advance="no") "  "
        end do
        write(error_unit,'(A,A)') "Exiting " // sub_name
        flush(error_unit)
      else if (trace_verbosity == 2) then
        write(error_unit,*) "TRACE: process ", trace_iproc, ": Exiting " // sub_name
        flush(error_unit)
      end if

      trace_time = trace_time + trace_timer() - trace_start

!$  end if

  end subroutine trace_exit

  !------------------------------------------------------------------------------!
    subroutine trace_get_time(sub_name, total_time, num_calls)                   !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   Gets the total runtime so far in the execution for a particular subroutine !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   character(len=*), intent(in)  :: sub_name                                  !
  !     Name of the subroutine to get the time for                               !
  !   real(kind=dp),    intent(out) :: total_time                                !
  !     Total time spent in that routine so far this run                         !
  !   real(kind=dp),    intent(out), optional :: num_calls                       !
  !     Total number of aclls made to that routine so far this run               !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    character(len=*), intent(in)  :: sub_name
    real(kind=dp),    intent(out) :: total_time
    integer,          intent(out), optional :: num_calls

    type(routine),  pointer       ::  current
    integer :: i

    ! Add the current subroutine to the times list and start the timer
    if(trace_timings) then
      current => times
      do
        if(current%id /= sub_name) then
          current => current%next
          cycle
        else if(current%end_time < 0.0_dp) then
          total_time = current%total_time
          if (present(num_calls)) num_calls = current%num_calls
          exit
        else
          write(*,*) "Error: Cannot get time for " // trim(sub_name) // "as this routine is currently running"
          stop
        end if
      end do
    end if

  end subroutine trace_get_time

  !------------------------------------------------------------------------------!
    subroutine trace_output(filename)                                            !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   Writes the timing information to a file                                    !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   character,  intent(in)  ::  filename                                       !
  !     file to write the trace information to                                   !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    implicit none

    character(len=*), intent(in)  :: filename

    type(routine),  pointer       ::  current
    integer                       ::  trace_unit, ierr
    logical                       ::  unit_ok, unit_open
    real(kind=dp)                 ::  run_time
    real(kind=dp)                 ::  comms_time

    if((.not. trace_enabled) .or. (.not. trace_timings)) return

    trace_start = trace_timer()

    call trace_exit('main')

    comms_time = 0.0_dp

    do trace_unit = 10, 100
      inquire(unit = trace_unit, exist = unit_ok, opened = unit_open)
      if(unit_ok .and. (.not. unit_open)) then
        open(unit=trace_unit, file=filename, action='write', position='rewind', status='replace', iostat=ierr)
        if(ierr /= 0) then
          write(*,*)"Unable to open file " // trim(filename) // " in trace_output"
          stop
        end if
        exit
      end if
    end do

    run_time = times%total_time

    call sort_list(times)

    current => times

    write(trace_unit,*) '+---------------------------------+-------------+-----------+-----------+'
    write(trace_unit,*) '| Subroutine name                 | Time (s)    | Time (%)  | Calls     |'
    write(trace_unit,*) '+---------------------------------+-------------+-----------+-----------+'
    do
      if((current%start_time < 0.0_dp) .and. (current%end_time < 0.0_dp)) then
        if(run_time > 0.0_dp) then
          write(trace_unit,'(A2,A32,A2,F12.5,A2,F10.3,A2,I10,A2)')                                                              &
          & '|',trim(current%id), ' |', current%total_time, ' |', current%total_time/run_time * 100, ' |', current%num_calls, ' |'
        else
          write(trace_unit,'(A2,A32,A2,F12.5,A2,F10.3,A2,I10,A2)')                                                              &
          & '|',trim(current%id), ' |', current%total_time, ' |', 0.0_dp, ' |', current%num_calls, ' |'
        end if
      else
        write(*,*)'Error: Subroutine ' // current%id // ' is still being profiled!'
        stop
      end if
      if(current%id(1:5) == 'comms') comms_time = comms_time + current%total_time
      if(associated(current%next)) then
        current => current%next
        cycle
      else
        exit
      end if
    end do
    write(trace_unit,*) '+---------------------------------+-------------+-----------+-----------+'

    trace_time = trace_time + trace_timer() - trace_start
    write(trace_unit, *)
    write(trace_unit, '(A21,F12.3,A8)') "Comms routines took:", comms_time, " seconds"
    write(trace_unit, '(A21,F12.3,A8)') "Trace routines took:", trace_time, " seconds"
    close(trace_unit)

  contains
    subroutine sort_list(list)
      implicit none

      type(routine),  pointer, intent(inout)  ::  list

      type(routine),  pointer ::  p, q, e, tail
      integer                 ::  i, insize, n_merges
      integer                 ::  psize, qsize

      insize = 1
      nullify(q)

      do
        p => list
        nullify(list)
        nullify(tail)
        n_merges = 0

        do while(associated(p))
          n_merges = n_merges + 1
          q => p
          psize = 0
          do i=1, insize
            psize = psize + 1
            q => q%next
            if(.not. associated(q)) exit
          end do

          qsize = insize

          do while((psize > 0) .or. ((qsize > 0) .and. associated(q)))
            if(psize == 0) then
              e => q
              q => q%next
              qsize = qsize - 1
            else if ((qsize == 0) .or. (.not. associated(q))) then
              e => p
              p => p%next
              psize = psize - 1
            else if(p%total_time > q%total_time) then
              e => p
              p => p%next
              psize = psize - 1
            else
              e => q
              q => q%next
              qsize = qsize - 1
            end if

            if(associated(tail)) then
              tail%next => e
            else
              list => e
            end if
            tail => e
          end do
          p => q
        end do
        !if(associated(tail)) nullify(tail%next)
        nullify(tail%next)

        if(n_merges <= 1) exit
        insize = insize * 2
      end do

    end subroutine sort_list

  end subroutine trace_output

  !------------------------------------------------------------------------------!
    subroutine trace_write_callstack(err_file)                                   !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   A subroutine to write the callstack of a program to err_file               !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   character,  intent(in)  ::  err_file                                       !
  !     file to write the call stack information to                              !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    implicit none

    integer,  intent(in)  ::  err_file
    type(routine), pointer  ::  current

    if(.not. trace_enabled) return

    write(err_file,*) "Call stack:"
    current => callstack
    do
      if(associated(current%next)) then
        write(err_file,*) "  " // trim(current%id)
        current => current%next
        cycle
      else
        write(err_file,*) "  " // trim(current%id)
        exit
      end if
    end do

  end subroutine trace_write_callstack

  !------------------------------------------------------------------------------!
    subroutine trace_finalise()                                                  !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   A subroutine to deallocate the timing and all stacks for the trace module  !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    implicit none

    type(routine),  pointer ::  current

    integer                 ::  ierr

    if(.not. trace_enabled) return

    ! Deallocate callstack
    current => callstack
    do
      if(associated(current%next)) then
        callstack => current%next
        deallocate(current, stat=ierr)
        if(ierr /= 0) then
          write(*,*)"Error deallocating current in trace_finalise"
          stop
        end if
        current => callstack
        cycle
      else
        deallocate(current, stat=ierr)
        if(ierr /= 0) then
          write(*,*) "Error deallocating current in trace_finalise"
          stop
        end if
        exit
      end if
    end do

    ! Deallocate timings stack
    if(trace_timings) then
      current => times
      do
        if(associated(current%next)) then
          times => current%next
          deallocate(current, stat=ierr)
          if(ierr /= 0) then
            write(*,*)"Error deallocating current in trace_finalise"
            stop
          end if
          current => times
          cycle
        else
          deallocate(current, stat=ierr)
          if(ierr /= 0) then
            write(*,*) "Error deallocating current in trace_finalise"
            stop
          end if
          exit
        end if
      end do
    end if

  end subroutine trace_finalise

  !------------------------------------------------------------------------------!
    subroutine routine_allocate(node, str)                                       !
  !------------------------------------------------------------------------------!
  ! DESCRPTION                                                                   !
  !   A subroutine to create a new routine node for the call/timing stacks     ` !
  !------------------------------------------------------------------------------!
  ! ARGUMENTS                                                                    !
  !   type(routine), pointer, intent(inout) :: node                              !
  !     Object containting information about the routine                         !
  !                                                                              !
  !   character,  intent(in)  ::  str                                            !
  !     String identifying the routine, usually the routine name                 !
  !------------------------------------------------------------------------------!
  ! AUTHORS                                                                      !
  !   Edward Higgins, 2014-09-17                                                 !
  !------------------------------------------------------------------------------!
    implicit none

    type(routine), pointer,  intent(inout) ::  node
    character(len=*),     intent(in)    ::  str

    integer     ::  ierr

    allocate(node, stat=ierr)
    if(ierr /= 0) then
      write(*,*) "Error allocating node in routine_allocate"
      stop
    end if

    node%id = str
    nullify(node%next)

    node%start_time = -1.0_dp
    node%end_time   = -1.0_dp
    node%total_time =  0.0_dp
    node%num_calls  =  0

  end subroutine routine_allocate

  real(kind=dp) function trace_timer()
    use omp_lib
    implicit none

    real          :: sngl_time
    real(kind=dp) :: time

    call cpu_time(sngl_time)
    time = real(sngl_time, kind=dp)
!$  time = omp_get_wtime()

    trace_timer = time
  end function trace_timer

end module trace

