program transpose_test
!==============================================================================#
! TRANSPOSE_TEST
!------------------------------------------------------------------------------#
! Author:  Edward Higgins <ed.higgins@york.ac.uk>
!------------------------------------------------------------------------------#
! Version: 0.1.1, 2020-01-27
!------------------------------------------------------------------------------#
! This code is distributed under the MIT license.
!==============================================================================#
  use constants
  use trace
  use arrays
  use communications
  use ISO_FORTRAN_ENV, only: output_unit
  implicit none


  type(complex_4d_array) :: A, AT

  integer :: sizes(4), transpose_sizes(4)
  character(len=32) :: layout, transpose_layout

  complex(kind=dp) :: A_elem, AT_elem

  integer :: i, j, k, l, ic
  integer :: bad_elements
  integer :: iproc, ierr



  call trace_initialise(enable_backtrace = .true., enable_timings = .true., verbosity = 0)
  call comms_initialise()
  call trace_set_iproc(comm_world%iproc(1))

  sizes  = [6,8,8,7]
  layout ="x,y;theta,phi"
  call array_create(A, sizes, layout)

  if (comm_world%on_root) then
    write(*,"('Global matrix size:      (',3(I02,','),I02,')')") sizes
    write(*,"('Global processor layout: ',a)") layout
    write(*,*) " "
  end if

  ic = 1+(comm_world%iproc(1)-1) * product(A%local_size)
  do l = A%local_start(4), A%local_end(4)
    do k = A%local_start(3), A%local_end(3)
      do j = A%local_start(2), A%local_end(2)
        do i = A%local_start(1), A%local_end(1)
          A%elements(i,j,k,l) = ic
          ic=ic+1
        end do
      end do
    end do
  end do

  transpose_sizes  = [sizes(3), sizes(4), sizes(1), sizes(2)]
  transpose_layout ="theta,phi;x,y"
  call array_create(AT, transpose_sizes, transpose_layout)

  if (A%distribution%on_root) then
    write(*,'("Array stored on ",I02," processes")') product(A%distribution%nprocs(1:))
    write(*,'("Process distribution is ",I02," x",I02)') A%distribution%nprocs(1:)
    write(*,'("Each process has a (",3(I02,","),I02,") array")') A%local_size
    write(*,'("There are ",I02," inactive processes")')  comm_world%nprocs(1) - product(A%distribution%nprocs(1:))
    write(*,*) " "
  end if

  if (AT%distribution%on_root) then
    write(*,'("Transpose stored on ",I02," processes")') product(AT%distribution%nprocs(1:))
    write(*,'("Process distribution is ",I02," x",I02)') AT%distribution%nprocs(1:)
    write(*,'("Each process has a (",3(I02,","),I02,") array")') AT%local_size
    write(*,'("There are ",I02," inactive processes")')  comm_world%nprocs(1) - product(AT%distribution%nprocs(1:))
    write(*,*) " "
  end if

  call array_transpose(A, AT)

  ! Loop over all elements and check that they have been correctly transposed
  bad_elements = 0
  do l = A%global_start(4), A%global_end(4)
    do k = A%global_start(3), A%global_end(3)
      do j = A%global_start(2), A%global_end(2)
        do i = A%global_start(1), A%global_end(1)

          A_elem  = element_on_root([i,j,k,l], A, tag=1)
          AT_elem = element_on_root([k,l,i,j], AT,tag=2)

          if (comm_world%on_root) then
            if (int(real(A_elem)) /= int(real(AT_elem))) then
              print *, "Found bad element:", A_elem, AT_elem
              bad_elements=bad_elements+1
            end if
          end if
          call comms_barrier(comm_world)
        end do
      end do
    end do
  end do

  if (comm_world%on_root) then
    if (bad_elements /= 0) then
      print *, "there were", bad_elements, "bad elements"
      call comms_abort("Transpose did not work")
    else
      print *, "Transpose was successful!"
    end if
  end if

  call comms_finalise()

contains

  function element_on_root(indices, M, tag)
    complex(kind=dp) :: element_on_root
    integer,                intent(in) :: indices(4)
    type(complex_4d_array), intent(in) :: M
    integer,                intent(in) :: tag

    integer :: i,j,k,l

    i = indices(1)
    j = indices(2)
    k = indices(3)
    l = indices(4)

    element_on_root = (-2, -2)
    if (comm_world%on_root) then
      if ( i >= M%local_start(1) .and. i <= M%local_end(1) .and. &
           j >= M%local_start(2) .and. j <= M%local_end(2) .and. &
           k >= M%local_start(3) .and. k <= M%local_end(3) .and. &
           l >= M%local_start(4) .and. l <= M%local_end(4) ) then
        element_on_root = M%elements(i,j,k,l)
      else
        call comms_recv(element_on_root, tag=tag)
      end if
    else
      if ( i >= M%local_start(1) .and. i <= M%local_end(1) .and. &
           j >= M%local_start(2) .and. j <= M%local_end(2) .and. &
           k >= M%local_start(3) .and. k <= M%local_end(3) .and. &
           l >= M%local_start(4) .and. l <= M%local_end(4) ) then
         call comms_send(M%elements(i,j,k,l), dest=1, tag=tag)
       end if
     end if
   end function element_on_root

end program transpose_test
