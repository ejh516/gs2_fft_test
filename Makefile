#=[ Project-dependent bits ]===============================

# Compiler choices/flags
FC 	= mpifort

#EJH# FCFLAGS = -march=x86-64 -O0 -g -fno-realloc-lhs -Wall -fcheck=all -I$(EBROOTFFTW)/include
FCFLAGS = -O3 -g -fno-realloc-lhs -Wall -I$(EBROOTFFTW)/include
#EJH# FCFLAGS = -O0 -g -nostandard-realloc-lhs
FLFLAGS = -lfftw3_mpi -lfftw3 -lm

# Programs and dependencies

PROGRAMS = serial_transpose_fft

TEST = comms_test arrays_test transpose_test gs2_test

all: $(PROGRAMS) $(TEST)

#=[ Generic bits ]=========================================

# Tell make to look in ./src and ./obj directories
vpath %.f90 src/
vpath %.mod obj/
vpath %.o   obj/

test: $(TEST)

fftw_mpi_test:

gs2_test: constants.o trace.o parameters.o communications.o arrays.o ffts.o regex.o

comms_test: communications.o trace.o

arrays_test: communications.o trace.o arrays.o

transpose_test: communications.o trace.o arrays.o

parameters.o: regex.o communications.o trace.o

ffts.o: trace.o parameters.o communications.o arrays.o

arrays.o: trace.o communications.o

communications.o: trace.o

trace.o: constants.o

# Programs require their object files
%: %.o
	$(FC) -o $@ $^ $(FCFLAGS) $(FLFLAGS)

# Object files require their source files
%.o: %.f90
	$(FC) $(FCFLAGS) -c $< -o $@

clean:
	rm -rf *.o *.mod $(PROGRAMS) $(TEST)

# vim:ft=make
